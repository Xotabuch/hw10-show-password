const form = document.querySelector(".password-form");
const btn = document.querySelector(".btn");

form.addEventListener("click", (e) => {
  if (e.target.tagName === "I") {
    const parent = e.target.closest("label");
    const input = parent.querySelector("input");
    const icons = parent.querySelectorAll(".icon-password");
    if (input.getAttribute("type") === "password") {
      input.type = "text";
    } else input.type = "password";
    icons.forEach((element) => {
      element.style.display = "none";
      if (element !== e.target) {
        element.style.display = "";
      }
    });
  }
});

btn.addEventListener("click", (e) => {
  e.preventDefault();
  const inputs = document.querySelectorAll("input");
  const error = document.querySelector("#error");
  if (
    inputs[0].value === inputs[1].value &&
    inputs[0].value !== "" &&
    inputs[1].value !== ""
  ) {
    if (document.body.contains(error)) {
      error.remove();
    }
    alert("You are welcome");
  } else {
    if (!document.body.contains(error)) {
      inputs[1].insertAdjacentHTML(
        "afterend",
        "<span id='error' style='color: red'>Нужно ввести одинаковые значения</span>"
      );
    }
  }
});
